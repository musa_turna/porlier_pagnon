#include <iostream>
using namespace std;

int main(){

	string marque;
	string modèle;
	Voiture voiture;

	// Les 2 fonctions sont "cout" et "cin"
	// cout demande de saisir la marque et le modele
	// cin rentre le modele et la marque dans le programme
	cout << "Saisir la marque";
	cin >> voiture.marque;
	cout << "Saisir le modèle";
	cin >> voiture.modèle;

	// cout affiche le prix trouvé
	cout << "Le prix de la voiture est " << voiture.prix
	
    return 0;
}